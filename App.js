// import boot from "./artifacts/boot/index";

// const app = boot();

// export default app;

import React from 'react'
import Route from "./src/index"
import {AppRegistry } from 'react-native';
import {name as appName} from './app.json';

export default class App extends React.Component {
  render() {
    return (
      <Route/>
    )
  }
}
AppRegistry.registerComponent(appName, () => App);

