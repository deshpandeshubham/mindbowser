import React from 'react';
import { Header, Left, Body, Right, Picker,Badge } from 'native-base';
import Icon from 'react-native-vector-icons/FontAwesome';
import { View, Text, TouchableOpacity, AsyncStorage } from 'react-native';
import styles from './styles';

class HeaderView extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      isopen: false,
      selected: '',
    };
  }

  toggleDrawer = () => {
    const { navigation } = this.props;
    navigation.toggleDrawer();
  };

  render() {
    const { navigation, title } = this.props;
    return (
      <Header
        androidStatusBarColor={"black"}
        style={styles.bgcolor}>
        <Left>
          <TouchableOpacity onPress={() => this.toggleDrawer()}>
            <Icon name="bars" size={30} color="black" />
          </TouchableOpacity>
        </Left>
        <Body>
    <Text style={styles.bodytext}>{title}</Text>
        </Body>
        <Right>
          <View style={styles.rightview}>
          {/* <TouchableOpacity>
            <Icon name="heart" size={30} color="red" />
          </TouchableOpacity> */}
          </View>
        </Right>
      </Header>

    );
  }
}

export default HeaderView;
