import {StyleSheet} from 'react-native';const styles = StyleSheet.create({
  container: {
    alignSelf: 'center',
    flex: 1,
    justifyContent: 'center',
    textAlign: 'left',
  },
  bgcolor: {
    backgroundColor: "#fff",
  },
  heading: {
    fontSize: 60,
    alignSelf: 'center',
    color: '#000099',
    fontWeight: 'bold',
  },

  bodytext: {
    color: 'black',
    fontSize: 15,
    right: 18,
    fontWeight: 'bold',
    top: -2,
  },

  rightview: {
    justifyContent: 'space-between',
    alignSelf: 'flex-end',
    flexDirection: 'row',
  },
  righttext: {
    left: 0,
    right: 10,
    alignSelf: 'center',
    color: '#626570',
  },

  dropdowntext: {
    fontSize: 18,
    fontWeight: 'bold',
    top: 1,
    color: '#4B4F52',
  },

  lngstyle: {
    fontSize: 20,
    fontWeight: 'bold',
  },

  pickerstyle: {
    height: 90,
    width: 90,
  },
});

export default styles;
