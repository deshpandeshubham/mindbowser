import React, { Component } from 'react';
import {
  View, ActivityIndicator, Modal, Text,AsyncStorage
} from 'react-native';
import styles from './styles';
import { NavigationEvents } from 'react-navigation';

export default class Loader extends Component {
  constructor(props) {
    super(props);
    this.state = {
    };
  }

  render() {
    const { loaderText, isModalVisible } = this.props;
    return (
      <Modal
        transparent={false}
        animationType={'none'}
        visible={isModalVisible}
      >
        
        <View style={styles.modalBackground}>
          <View style={styles.activityIndicatorWrapper}>
            <ActivityIndicator size="large" color="#48372f" />
            {
              <Text style={{ color: "black",textAlign:"center" }}>Please Wait</Text>
            }
          </View>
        </View>
      </Modal>
    );
  }
}
