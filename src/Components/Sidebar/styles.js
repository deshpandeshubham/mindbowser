import {StyleSheet} from 'react-native';
const styles = StyleSheet.create({
  AvatorContainer: {
    backgroundColor: "#FFF",
  },
  sideMenuContainer: {
   
    backgroundColor: '#fff',
    alignItems: 'center',
    paddingTop: 10,
  },

  imagcircle: {
    height: 150,
    width: 150,
    borderRadius: 150 / 2,
    backgroundColor: "#FFF",
    alignItems: 'center',
  },
  edit: {
    color: "#FFF",
    fontSize: 15,
    borderBottomColor: "#FFF",
    borderWidth: 1,
    borderTopWidth: 0,
    borderLeftWidth: 0,
    borderRightWidth: 0,
  },
  font: {
    fontWeight: 'bold',
    fontSize: 25,
  },
  menucontainer: {
    flexDirection: 'row',
    alignItems: 'center',

    paddingBottom: 5,
    backgroundColor:"#FFF",
    width: '100%',
    paddingTop: 15,
  },
  menustyle: {
    fontSize: 15,
    fontWeight: 'normal',
    paddingLeft: 20,
  },
  sidebariconview: {
    marginRight: 10,
    marginLeft: 20,
  },
  Avatarmainstyle: {
    paddingTop: 20,
    paddingBottom: 20,
  },
  businessname: {
    alignSelf: 'center',
    top: 10,
  },
  edittext: {
    alignSelf: 'center',
    flexDirection: 'row',
    top: 10,
  },
  separtor: {
    width: '100%',
    backgroundColor: '#3E4EA5',
    marginTop: 15,
  },
  row: {
    flexDirection: 'row',
    marginBottom: 5,
    width: '100%',
  },
  image:{
    height:80,
    width:80,
    borderWidth:50/2
  },
  activeBackgroundColor: {
    fontWeight: 'bold',
    color: '#2132a3',
    width: '100%',
  },
  profileImage:{
    width: 120, 
    height: 120, 
    borderRadius: 120 / 2
  },
  white: {
    color: "#FFF",
  },
  Menumainview: {
    width: '100%',
    height: '100%',
    backgroundColor: '#fff',
    marginTop: 15,
  },
  left10: {
    left: 10,
  },
  top20: {
    top: 20,
  },
  editbtn: {
    color: '#fff',
  },
});

export default styles;
