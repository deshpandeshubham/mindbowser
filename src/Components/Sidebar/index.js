import React, { Component } from 'react';
import { View, Text, StatusBar, TouchableOpacity, Alert,Image,BackHandler } from 'react-native';
import Icon from 'react-native-vector-icons/FontAwesome';
import styles from './styles';
import { NavigationActions } from 'react-navigation';
class Sidebar extends Component {
  constructor(props) {
    super(props);
    this.state = {
      name: '',
      profileImage: '',
      expiryDate:'',
    };
  }

  navigateToScreen = (route, action) => () => {
    const navigate = NavigationActions.navigate({
      routeName: route,
    });
    this.props.navigation.dispatch(navigate);
  };

  logOut() {
    Alert.alert(
      'Logout',
      'Are you sure you want to logout?', [{
        text: 'Cancel',
        onPress: () => { },
        style: 'cancel',
      }, {
        text: 'OK',
        onPress: () => this.logoutUser(),
      }], {
      cancelable: false,
    },
    );
  }

  



  logoutUser() {
    const { navigation } = this.props;
    BackHandler.exitApp()    
  }

  render() {
    const { navigation } = this.props;
    return (
      <View style={styles.sideMenuContainer}>
        <StatusBar
          barStyle="light-content"
          hidden={false}
          backgroundColor={"black"}
        />
        <View style={styles.top20}>
          <Image style={styles.profileImage} source = {require('../../../assets/1.jpg')} />
        </View>
        <View style={styles.Avatarmainstyle}>
          <View style={styles.businessname}>
            <Text style={styles.font}>Test User</Text>
          </View>
          <View style={styles.edittext}>
          <Text style={styles.edit} onPress={() => navigation.navigate('EditUserProfile')}>Edit</Text>
          </View>
        </View>
        <View style={styles.Menumainview}>
          <View style={styles.menucontainer}>
            <TouchableOpacity
              onPress={this.navigateToScreen('Home')}
              style={[
                styles.row,
                this.props.activeItemKey === 'Home'
                  ? styles.activeBackgroundColor
                  : null,
              ]}>
              <View style={styles.sidebariconview}>
                <Icon
                  name={'home'}
                  size={30}
                  style={[
                    this.props.activeItemKey === 'Home'
                      ? styles.activeBackgroundColor
                      : null,
                  ]}
                />
              </View>
              <Text
                style={[
                  styles.menustyle,
                  this.props.activeItemKey === 'Home'
                    ? styles.activeBackgroundColor
                    : null,
                ]}>
                Home
              </Text>
            </TouchableOpacity>

          </View>
      
          <View style={styles.menucontainer}>
            <TouchableOpacity
              onPress={this.navigateToScreen('Favourite')}
              style={[
                styles.row,
                this.props.activeItemKey === 'Favourite'
                  ? styles.activeBackgroundColor
                  : null,
              ]}>
              <View style={styles.sidebariconview}>
                <Icon
                  name={'heart'}
                  size={30}
                  style={[
                    this.props.activeItemKey === 'Favourite'
                      ? styles.activeBackgroundColor
                      : null,
                  ]}
                />
              </View>
              <Text
                style={[
                  styles.menustyle,
                  this.props.activeItemKey === 'Favourite'
                    ? styles.activeBackgroundColor
                    : null,
                ]}>
                 Favourite List   
              </Text>
            </TouchableOpacity>

          </View>
          <View style={styles.menucontainer}>
            <TouchableOpacity
              style={styles.row}
              onPress={() => this.logOut()}
            >
              <View style={styles.sidebariconview}>
                <Icon name={'close'} size={30} />
              </View>
              <Text style={styles.menustyle}>Logout</Text>
            </TouchableOpacity>
          </View>

        </View>
      </View>
    );
  }
}

export default Sidebar;
