import React from 'react'
import { createStackNavigator, createAppContainer, createDrawerNavigator, createSwitchNavigator } from "react-navigation";
import Splash from "./Screen/splash/";
import Home from "./Screen/Home/";
import Details from "./Screen/Details/"
import Favourite from "./Screen/favourite/"
import SidebarView from "./Components/Sidebar/"

const app = createStackNavigator(
    {
      Home: {screen : Home} ,
      Details: {screen : Details},
      Favourite:{screen : Favourite}
    },
    {
      initialRouteName: 'Home',
      headerMode: 'none',
    },
  );
  
  const Route = createDrawerNavigator(
    {
      Drawer:app
    },
    {
      contentComponent:SidebarView,
    },
    {
      drawerLockMode: 'locked-opened',
      disableGestures: true,
      
    },
  );
  
  
  
  
  
  
  const RootNavigator = createSwitchNavigator(
    {
      Splash: { screen: Splash },
      app: { screen: Route },
    },
    {
      initialRouteName: 'Splash',
    },
  

)




const AppContainer = createAppContainer(RootNavigator);


export default AppContainer;
