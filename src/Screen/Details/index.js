import React, { Component } from "react";
import {
  Text,
  View,
  StatusBar,
  ActivityIndicator,
  ScrollView,
  TouchableOpacity,
  Image,
} from "react-native";
import {
  Container,
  Content,
  Card,
  Toast,
  CardItem,
  Left,
  Body,
  Button,
  Icon,
  Right,
} from "native-base";
import styles from "./styles";
import HeaderView from "../../Components/Header/";
import GLOBALS from "../../boot/global";
import Loader from "../../Components/Loader/";

class Details extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      collection: [],
      showLoader: false,
      title: "",
      image: "",
      decription: "",
      date: "",
    };
  }

  componentDidMount() {
    this.setState({
      showLoader: true,
    });
    const { navigation } = this.props;
    const id = navigation.getParam("id");
    fetch(GLOBALS.BASE_URL + `gifs/${id}?api_key=${GLOBALS.Api_key}`, {
      method: "GET",
    })
      .then((response) => response.json())
      .then((responseJson) => {
        if (responseJson.meta.status == 200) {
          this.setState({
            showLoader: false,
            title: responseJson.data.title,
            date: responseJson.data.import_datetime,
            image: responseJson.data.images.downsized_large.url,
            decription: responseJson.data.username,
          });
        } else {
          this.setState({
            showLoader: false,
          });
          this.refs.toast.show(message); //eslint-disable-line
        }
      })
      .catch(() => {
        this.setState({
          showLoader: false,
        });
        this.refs.toast.show("Something went Wrong"); //eslint-disable-line
      });
  }

  render() {
    const { showLoader, title, image, date, decription } = this.state;
    const { navigation } = this.props;
    return (
      <Container>
        <HeaderView navigation={navigation} title={"Stickers Collection"} />
        {showLoader && <Loader />}
        <Card>
          <CardItem>
            <Left>
              <Body>
                <Text>{title}</Text>
              </Body>
            </Left>
            <Right>
              <Text>BY:{decription}</Text>
            </Right>
          </CardItem>
          <CardItem cardBody>
            <Image
              source={{ uri: image }}
              style={{ height: 200, width: null, flex: 1 }}
            />
          </CardItem>
          <CardItem>
            <Right>
              <Text>{date}</Text>
            </Right>
          </CardItem>
        </Card>
        <Toast
          ref="toast" //eslint-disable-line
          defaultCloseDelay={500}
        />
      </Container>
    );
  }
}
export default Details;
