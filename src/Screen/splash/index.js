import React, { Component } from 'react';
import { Text, View, StatusBar, ActivityIndicator } from 'react-native';
import Icon from 'react-native-vector-icons/FontAwesome';
import styles from "./styles"
class Splash extends React.Component {
  componentDidMount() {
    setTimeout(() => {
      const { navigation } = this.props
      navigation.navigate("app");
    }, 2000)
  }         
    render() {
    return (
      <View style={styles.container}>
        <StatusBar hidden={true} />
        <Icon name="smile-o" size={50} color="#fff" />
        <Text style={styles.heading}>Welcome to Stickers Collections</Text>
        <View style={{ top: 20 }}>
          <ActivityIndicator color={'#fff'} size='large' />
        </View>
      </View>
    );
  }
}
export default Splash;