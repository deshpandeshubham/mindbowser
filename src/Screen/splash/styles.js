import React, { Component } from 'react';
import { Platform, StyleSheet,} from 'react-native';

const styles=StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#324aa8',
  },
  heading:{
    fontSize: 20,
    color: "#fff",
    fontFamily:'roboto-thin'
  }

})
export default styles;