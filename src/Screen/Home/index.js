import React, { Component } from "react";
import {
  Text,
  View,
  ScrollView,
  AsyncStorage,
  TouchableOpacity,
  Image,
  Alert
} from "react-native";
import {
  Container,
  Content,
  Card,
  Toast,
  CardItem,
  Left,
  Thumbnail,
  Body,
  Button,
  Icon,
  Right,
} from "native-base";
import styles from "./styles";
import HeaderView from "../../Components/Header/";
import GLOBALS from "../../boot/global";
import Loader from "../../Components/Loader/";

class Home extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      collection: [],
      showLoader: false,
      currentPage: 0,
      hasMore: true,
      pageLimit: 5,
      product: {},
    };
  }

  getData = async (displayLoader = false) => {
    if (displayLoader) this.setState({ showLoader: true });
    const { currentPage, pageLimit, collection } = this.state;
    let { hasMore } = this.state;
    fetch(
      GLOBALS.BASE_URL +
        `gifs/trending?api_key=${GLOBALS.Api_key}&offset=${currentPage}&limit=${pageLimit}&rating=g`,
      {
        method: "GET",
      }
    )
      .then((response) => response.json())
      .then((responseJson) => {
        if (responseJson.meta.status == 200) {
          if (responseJson.pagination) {
            if (responseJson.pagination.total_count == currentPage) {
              this.setState({
                hasMore: false,
              });
            }
          }
          this.setState({
            showLoader: false,
            collection: [...collection, ...responseJson.data],
            currentPage: currentPage + 1,
            hasMore,
          });
        } else {
          this.refs.toast.show(message); //eslint-disable-line
        }
      })
      .catch(() => {
        this.refs.toast.show("Something went Wrong"); //eslint-disable-line
      });
  };

  componentDidMount() {
    this.setState({
      showLoader: true,
    });
    this.getData(true);
  }

  renderItem(item, index) {
    const { collection } = this.state;
    const { navigation } = this.props;
    return (
      <View style={styles.width50}>
        <View style={styles.maincontainerview}>
          <Content>
            {collection &&
              collection.map((item, index) => (
                <React.Fragment>
                  <TouchableOpacity onPress={() => navigation.navigate('Details', {id:item.id})}>
                  <Card>
                    <CardItem>
                      <Left>
                        <Body>
                          <Text>{item.title}</Text>
                        </Body>
                      </Left>
                    </CardItem>
                    <CardItem cardBody>
                      <Image
                        source={{ uri: item.images.downsized_large.url }}
                        style={{ height: 200, width: null, flex: 1 }}
                      />
                    </CardItem>
                    <CardItem>
                      <Left>
                        <Button transparent>
                            <TouchableOpacity style={{ flexDirection: "row" }} onPress={() => this.favourite(item)}>
                            <Icon active name="thumbs-up"/>
                            <Text style={{ left: 5 }}>Add Favorite</Text>
                            </TouchableOpacity>
                        </Button>
                      </Left>
                      <Right>
                        <Text>{item.import_datetime}</Text>
                      </Right>
                    </CardItem>
                  </Card>
                  </TouchableOpacity>
                </React.Fragment>
              ))}
          </Content>
        </View>
      </View>
    );
  }

  isCloseToBottom({ layoutMeasurement, contentOffset, contentSize }) {
    // eslint-disable-line
    return (
      layoutMeasurement.height + contentOffset.y >= contentSize.height - 500
    );
  }

  favourite(item) {
    Alert.alert(
      'Add favourite',
      'Do you want to Add in favourite?', [{
        text: 'Cancel',
        onPress: () => { },
        style: 'cancel',
      }, {
        text: 'OK',
        onPress: () => this.addFavourite(item),
      }], {
      cancelable: false,
    },
    );
  }

  



  addFavourite(item) {
    const { navigation } = this.props;
    let obj ={}
    obj.title=item.title;
    obj.image=item.images.downsized_large.url ;
    if(GLOBALS.favourite.length > 5){
      Alert.alert('More 5 entries is Not allowed')
    }
    else{
      GLOBALS.favourite.push(obj)
      Alert.alert('Gif Added into Favourite')
    }
  }


  render() {
    const { showLoader, hasMore,collection } = this.state;
    const { navigation } = this.props;
    return (
      <Container>
        <HeaderView navigation={navigation} title={"Stickers Collection"} />
        {showLoader && <Loader />}
        <ScrollView
          bounces={false}
          keyboardShouldPersistTaps="handled"
          onScroll={({ nativeEvent }) => {
            if (this.isCloseToBottom(nativeEvent) && hasMore) {
              this.getData();
            }
          }}
        >
          {this.renderItem()}
        </ScrollView>
        {collection.length === 0 && (
          <View style={styles.NoDatacontainer}>
            <Text>No Data found</Text>
          </View>
        )
        }
        <Toast
          ref="toast" //eslint-disable-line
          defaultCloseDelay={500}
        />
      </Container>
    );
  }
}
export default Home;
