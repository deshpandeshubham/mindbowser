import React, { Component } from "react";
import {
  Text,
  View,
  StatusBar,
  ActivityIndicator,
  ScrollView,
  AsyncStorage,
  TouchableOpacity,
  Image,
  Alert
} from "react-native";
import {
  Container,
  Content,
  Card,
  Toast,
  CardItem,
  Left,
  Thumbnail,
  Body,
  Button,
  Icon,
  Right,
} from "native-base";
import HeaderView from "../../Components/Header/";
import GLOBALS from "../../boot/global";
import styles from "./styles" 
import Loader from "../../Components/Loader/";

class Favourite extends React.Component {



  renderItem(item, index) {
    const { navigation } = this.props;
    return (
      <View>
        <View>
          <Content>
            {GLOBALS.favourite &&
              GLOBALS.favourite .map((item, index) => (
                <React.Fragment>
                  <Card>
                    <CardItem>
                      <Left>
                        <Body>
                          <Text>{item.title}</Text>
                        </Body>
                      </Left>
                    </CardItem>
                    <CardItem cardBody>
                      <Image
                        source={{ uri: item.image }}
                        style={{ height: 200, width: null, flex: 1 }}
                      />
                    </CardItem>
                  </Card>
                </React.Fragment>
              ))}
          </Content>
        </View>
      </View>
    );
  }


  render() {
    const { navigation } = this.props;
    return (
      <Container>
        <HeaderView navigation={navigation} title={"Stickers Collection"} />
        <ScrollView
          bounces={false}
          keyboardShouldPersistTaps="handled"
        >
          {this.renderItem()}
        </ScrollView>
        {GLOBALS.favourite.length === 0 && (
          <View style={styles.NoDatacontainer}>
            <Text>No Data found</Text>
          </View>
        )

        }
        <Toast
          ref="toast" //eslint-disable-line
          defaultCloseDelay={500}
        />
      </Container>
    );
  }
}
export default Favourite;
